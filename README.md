# Welcome

Related article https://dev.to/stack-labs/securing-the-connectivity-between-amazon-eks-and-amazon-rds-part-1-527o

# Prerequisites

aws cli
kubectl
kustomize
psql

```shell
export ENV=<ENV>
export REGION=eu-west-1
export EKS_CLUSTER_NAME=eks-cluster-$ENV
export AWS_PROFILE=<AWS_PROFILE>
export INTERNAL_IP_RANGE=<LOCAL_OR_INTERNAL_IP_RANGES>
export TERRAFORM_BUCKET_NAME=<BUCKET_NAME>
```

# Create bucket for terraform states

```shell
# Create bucket
aws s3api create-bucket \
     --bucket $TERRAFORM_BUCKET_NAME \
     --region $REGION \
     --create-bucket-configuration LocationConstraint=$REGION

# Make it not public     
aws s3api put-public-access-block \
    --bucket $TERRAFORM_BUCKET_NAME \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

# Enable versioning
aws s3api put-bucket-versioning \
    --bucket $TERRAFORM_BUCKET_NAME \
    --versioning-configuration Status=Enabled
```

# Run terraform

```shell
cd infra/envs/dev

sed -i "s,<INTERNAL_IP_RANGE>,$INTERNAL_IP_RANGE,g" terraform.tfvars

terraform init \
    -backend-config="bucket=$TERRAFORM_BUCKET_NAME" \
    -backend-config="key=$ENV/terraform-state" \
    -backend-config="region=$REGION" \
../../plan/ 

terraform apply ../../plan/ 
```

## Create Metabase user

```shell
psql --host $(terraform output public-rds-endpoint) --port 5432 --user $(terraform output rds-username) --dbname postgres
CREATE USER metabase;
GRANT rds_iam TO metabase;
CREATE DATABASE metabase;
GRANT ALL ON DATABASE metabase TO metabase;
```

# Connecto to EKS

```shell
aws eks --region $REGION update-kubeconfig --name $EKS_CLUSTER_NAME
```

# Enable pods to receive their own network interfaces

Update your CNI version  

```shell
curl -o aws-k8s-cni.yaml https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/v1.7.9/config/v1.7/aws-k8s-cni.yaml
sed -i "s/us-west-2/$REGION/g" aws-k8s-cni.yaml
kubectl apply -f aws-k8s-cni.yaml
# Setting ENABLE_POD_ENI to true will add the vpc.amazonaws.com/has-trunk-attached label to the node if it is possible to attach an additional ENI. This is an essential configuration required to enable SGs for pods.
kubectl set env daemonset -n kube-system aws-node ENABLE_POD_ENI=true
kubectl set env daemonset -n kube-system aws-node AWS_VPC_K8S_CNI_EXTERNALSNAT=true
```

# Deploy K8S apps

Fill values

```shell
cd config/envs/dev
# Generate temporary password
METABASE_PWD=$(aws rds generate-db-auth-token --hostname $(terraform output private-rds-endpoint) --port 5432 --username metabase --region $REGION)
METABASE_PWD=$(echo -n $METABASE_PWD | base64 -w 0 )
sed -i "s/<MB_DB_PASS>/$METABASE_PWD/g" database-secret.patch.yaml
sed -i "s/<POD_SECURITY_GROUP_ID>/$(terraform output sg-rds-access)/<EKS_CLUSTER_SECURITY_GROUP_ID>/$(terraform output sg-eks-cluster)/g" security-group-policy.patch.yaml
sed -i "s,<RDS_ACCESS_ROLE_ARN>,$(terraform output rds-access-role-arn),g" service-account.patch.yaml
sed -i "s/<MB_DB_HOST>/$(terraform output private-rds-endpoint)/g" deployment.patch.yaml
```

Run manifests

```shell
kubectl create namespace metabase
kubectl config set-context --current --namespace=metabase
kustomize build . | kubectl apply -f -
```

# Clean

```shell
kustomize build . | kubectl delete -f -
terraform destroy ../../plan/ 
```

# Documentation

* https://aws.amazon.com/blogs/database/best-practices-for-securing-sensitive-data-in-aws-data-stores/
* https://aws.amazon.com/blogs/database/applying-best-practices-for-securing-sensitive-data-in-amazon-rds/
* https://aws.amazon.com/blogs/opensource/introducing-fine-grained-iam-roles-service-accounts/
* https://aws.amazon.com/blogs/containers/introducing-security-groups-for-pods/
* https://aws.amazon.com/fr/blogs/networking-and-content-delivery/using-static-ip-addresses-for-application-load-balancers/
* https://medium.com/dev-genius/create-an-amazon-eks-cluster-with-managed-node-group-using-terraform-a3b50d276b13
* https://medium.com/swlh/amazon-eks-and-security-groups-for-pods-759129f82dae
