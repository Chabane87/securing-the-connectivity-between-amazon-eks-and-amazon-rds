env               = "dev"
vpc_cidr_block    = "10.0.0.0/16"
internal_ip_range = "<INTERNAL_IP_RANGE>"
az                = ["eu-west-1a", "eu-west-1b"]
